 FROM node:16.15.0

 WORKDIR /app

 COPY package.json ./

 RUN yarn install
 
 RUN yarn info && yarn upgrade && yarn add yarn

 COPY . .

 CMD ["yarn", "start"]


