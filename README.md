## Criação e configuração do Dockerfile

Inicialmente escolhi a versão da imagem node:16.15.0 para a execução do projeto..

Em seguida criei o diretorio /app.

Copiei da minha raiz do projeto o package.json para diretorio /app.

Executando os comando yarn install, yarn info && yarn upgrade && yarn add yarn, para instalação de dependências do projeto e resolver conflitos e atualizar.

Agora utilizando o COPY estou copiando todos os meus arquivos da raiz do projeto para o diretorio /app.

Logo após utilizando o comando para execução do projeto com o CMD ["yarn", "start"].


### Build do Dockerfile

Ao finalizar o Dockerfile fiz o build executando o seguinte comando docker build -t nomedaimagem .

E subir um container local de teste para verificar se estava tudo certo com a aplicação. (docker run -p 3000:3000 -d nomedaimagem)

Após testar estava tudo ok.

### Criação do .gitlab-ci

Nessa etapa eu utilizei uma imagem do docker:stable para um service(docker:dind) que precisava utilizar a imagem do docker.

O gitlab-ci é composto por dois stages o build e deploy.

Utilizei duas variaveis de ambiente para logar no meu dockerhub $CI_REGISTRY_USER, $CI_REGISTRY_PASSWORD. Essas variaveis configurei no meu projeto do gitlab em configurações em CI/CD passando o login do meu dockerhub.

`stage: build`
Após logar fiz o build da imagem, criei tambem a tag latest como solicitado e fiz o push para meu repositorio do Dockerhub.

`stage: deploy`
Nesse Stage utilizei uma nova variável de ambiente $HEROKU_API_KEY está responsável por fazer a comunicação com a Heroku.

Novamente fiz o login no Dockerhub através das variáveis de ambiente.

Fiz o pull da minha imagem que está no Dockerhub.

Criei uma nova tag dessa imagem para o meu projeto no heroku utilizando o seguinte formato - registry.heroku.com/nomedomeuapp/web.

Em seguida realizei o login no meu Heroku com a variável de ambiente $HEROKU_API_KEY.

Após logado fiz o push registry.heroku.com/nomedomeuapp/web.

por ultimo execuntando o realizando a autenticação de login da Heroku e execução da cli e passando o comando de container:realease web --app nomedomeuapp.


### Links

imagem Dockerhub - https://hub.docker.com/repository/docker/caio10/desafioreact

deploy heroku - https://reactjsdesafio.herokuapp.com/

gitlab - https://gitlab.com/CaioVinicius10/desafioreactjs
